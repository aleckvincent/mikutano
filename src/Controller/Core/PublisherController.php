<?php

namespace App\Controller\Core;

use App\Entity\Core\Publisher;
use App\Exception\MissingMandatoryPropertyException;
use App\Exception\ResourceNotFoundException;
use App\Service\PublisherServiceInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route("/api/publishers")
 */
class PublisherController extends AbstractFOSRestController
{

    protected PublisherServiceInterface $service;

    public function __construct(PublisherServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @Rest\Post()
     * @ParamConverter("publisher", converter="fos_rest.request_body")
     */
    public function save(Publisher $publisher): View
    {
        try {
            return View::create($this->service->save($publisher), Response::HTTP_CREATED);
        }
        catch (MissingMandatoryPropertyException $mandatoryPropertyException) {
            return View::create($mandatoryPropertyException->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Put("/{id}")
     * @Rest\View(serializerGroups={"show_publisher", "list_publisher"})
     */
    public function update(int $id, Request $request): View
    {
        $data = $request->getContent();
        try {
            return View::create($this->service->update($id, $data), Response::HTTP_OK);
        }
        catch (MissingMandatoryPropertyException $mandatoryPropertyException) {
            return View::create($mandatoryPropertyException->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        catch (ResourceNotFoundException $exception) {
            return View::create($exception->getMessage(), Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Rest\Get
     * @Rest\View(serializerGroups={"show_publisher", "list_publisher"})
     */
    public function list(): View
    {
        return View::create($this->service->list(), Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/{id}")
     * @Rest\View(serializerGroups={"show_publisher", "list_publisher"})
     */
    public function show(Publisher $publisher): View
    {
        return View::create($publisher, Response::HTTP_OK);
    }

    /**
     * @Rest\Delete("/{id}")
     */
    public function delete(Publisher $publisher): View
    {
        $this->service->remove($publisher);
        return View::create('Removed', Response::HTTP_OK);
    }

}
