<?php

namespace App\Controller\Core;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class FrontendController extends AbstractController
{

    /**
     * @Route("/app/{vueRouting}", name="app_vue", methods={"GET"}, requirements={"vueRouting"=".*"})
     */
    public function app(): Response
    {
        return $this->render('app.html.twig');
    }

    /**
     * @Route("/")
     */
    public function index(): RedirectResponse
    {
        return $this->redirectToRoute('app_vue', [
            'vueRouting' => 'dashboard'
        ]);
    }

}
