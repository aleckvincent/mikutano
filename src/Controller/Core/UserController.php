<?php

namespace App\Controller\Core;

use App\Entity\Core\User;
use App\Exception\MissingMandatoryPropertyException;
use App\Exception\PasswordLengthException;
use App\Exception\ResourceNotFoundException;
use App\Service\impl\UserService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;


/**
 * @author Aleck VINCENT-MINATCHY <aleck.vincent@gmail.com>
 * @Rest\Route("/api/users")
 */
class UserController extends AbstractFOSRestController
{

    protected UserService $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }


    /**
     * @Rest\Post()
     * @ParamConverter("user", converter="fos_rest.request_body")
     * @Rest\View(serializerGroups={"show"})
     */
    public function register(User $user) : View
    {
        try {
            return View::create($this->service->register($user), Response::HTTP_CREATED);
        }
        catch (TransportExceptionInterface $transportException) {
            return View::create('The user was created but we occurred an error during the e-mail transport. 
            Here is the message : ' . $transportException->getMessage(), Response::HTTP_OK);
        }
        catch (MissingMandatoryPropertyException $mandatoryPropertyException) {
            return View::create($mandatoryPropertyException->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/activate/{token}")
     * @Rest\View(serializerGroups={"show"})
     */
    public function activate(string $token, Request $request) : View
    {
        $requestDecoded = json_decode($request->getContent());
        try {
            $response = $this->service->activate($token, $requestDecoded->password);
            $this->addFlash('success', 'Le compte été activé. Tu peux dès à présent te connecter.');
            return View::create($response, Response::HTTP_OK);
        }
        catch (ResourceNotFoundException $exception) {
            return View::create($exception->getMessage(), Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Rest\Post("/forgot/ask")
     * @Rest\View(serializerGroups={"show"})
     */
    public function askForgot(Request $request) : View
    {
        $body = json_decode($request->getContent());
        try {
            return View::create($this->service->askForgotPassword($body->email), Response::HTTP_OK);
        }
        catch (ResourceNotFoundException $exception) {
            return View::create($exception->getMessage(), Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Rest\Post("/forgot/reset/{forgotToken}")
     * @Rest\View(serializerGroups={"forgot_password"})
     */
    public function resetForgotPassword(string $forgotToken, Request $request) : View
    {
        $body = json_decode($request->getContent());
        try {
            return View::create($this->service->resetForgotPassword($body->password, $forgotToken), Response::HTTP_OK);
        }
        catch (ResourceNotFoundException $exception) {
            return View::create($exception->getMessage(), Response::HTTP_NOT_FOUND);
        }
        catch (PasswordLengthException $passException) {
            return View::create($passException->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * @Rest\Put("/{id}")
     * @Rest\View(serializerGroups={"show"})
     */
    public function update(int $id, Request $request): View
    {
        $data = $request->getContent();

        try {
            return View::create($this->service->update($id, $data), Response::HTTP_OK);
        }
        catch (ResourceNotFoundException $exception) {
            return View::create($exception->getMessage(), Response::HTTP_NOT_FOUND);
        }
        catch (MissingMandatoryPropertyException $manException) {
            return View::create($manException->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Delete("/{id}")
     * @Rest\View(serializerGroups={"show"})
     */
    public function delete(User $user): View
    {
        $this->service->delete($user);

        return View::create('User deleted.', Response::HTTP_OK);
    }

    /**
     * @Rest\Get()
     * @Rest\View(serializerGroups={"show"})
     */
    public function list(): View
    {
        return View::create($this->service->list(), Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/{id}")
     * @Rest\View(serializerGroups={"show"})
     */
    public function show(User $user): View
    {
        return View::create($this->service->show($user), Response::HTTP_OK);
    }

}
