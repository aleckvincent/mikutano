<?php

namespace App\Controller\Weekend;

use App\Entity\Weekend\Speech;
use App\Repository\Weekend\SpeechRepository;
use App\Service\Weekend\SpeechServiceInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route("/api/speeches")
 */
class SpeechController extends AbstractFOSRestController
{

    protected SpeechServiceInterface $service;

    public function __construct(SpeechServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @Rest\Get
     */
    public function list(Request $request): View
    {
        $key = $request->query->get('key');
        return View::create($this->service->autocomplete($key), Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/{id}")
     */
    public function update(Speech $speech, Request $request): View
    {
        $data = $request->getContent();

        return View::create($this->service->update($speech, $data), Response::HTTP_OK);

    }

    /**
     * @Rest\Post()
     * @ParamConverter("speech", converter="fos_rest.request_body")
     */
    public function save(Speech $speech, SpeechRepository $repository): View
    {
        $repository->add($speech, true);
        return View::create($speech, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/{id}")
     */
    public function show(Speech $speech): View
    {
        return View::create($speech, Response::HTTP_OK);
    }

}
