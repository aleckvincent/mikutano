<?php

namespace App\Controller\Weekend;

use App\Entity\Weekend\Privilege;
use App\Exception\ResourceNotFoundException;
use App\Repository\Weekend\PrivilegeRepository;
use App\Service\Weekend\PrivilegeServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route("/api/privileges")
 */
class PrivilegeController extends AbstractFOSRestController
{

    private PrivilegeServiceInterface $service;

    /**
     * @param PrivilegeServiceInterface $service
     */
    public function __construct(PrivilegeServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @Rest\Post("/publisher/{publisherId}")
     * @ParamConverter("privilege", converter="fos_rest.request_body")
     */
    public function save(int $publisherId, Privilege $privilege): View
    {
        try {
            return View::create($this->service->save($publisherId, $privilege), Response::HTTP_OK);
        }
        catch (ResourceNotFoundException $e) {
            return View::create($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Rest\View(serializerGroups={"show", "show_privilege"})
     * @Rest\Put("/{id}")
     */
    public function update(Privilege $privilege, Request $request): View
    {
        $data = $request->getContent();

        return View::create($this->service->update($privilege, $data), Response::HTTP_OK);
    }

    /**
     * @Rest\View(serializerGroups={"show", "show_privilege"})
     * @Rest\Get()
     */
    public function list(PrivilegeRepository $repository): View
    {
        return View::create($repository->findAll(), Response::HTTP_OK);
    }

    /**
     * @Rest\Delete("/{id}")
     */
    public function delete(Privilege $privilege, EntityManagerInterface $em): View
    {
        $em->remove($privilege);
        $em->flush();
        return View::create('Deleted', Response::HTTP_OK);
    }


}
