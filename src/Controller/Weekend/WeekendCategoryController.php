<?php

namespace App\Controller\Weekend;

use App\Entity\Weekend\WeekendCategory;
use App\Repository\Weekend\WeekendCategoryRepository;
use App\Service\Weekend\impl\WeekendCategoryService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route("/api/weekend/categories")
 */
class WeekendCategoryController extends AbstractFOSRestController
{

    /**
     * @Rest\Get()
     */
    public function list(WeekendCategoryRepository $repository): View
    {
        return View::create($repository->findAll(), Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/{id}")
     */
    public function show(WeekendCategory $weekendCategory): View
    {
        return View::create($weekendCategory, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/{id}")
     */
    public function update(WeekendCategory $weekendCategory, Request $request, WeekendCategoryService $service): View
    {
        $data = $request->getContent();
        return View::create($service->update($weekendCategory, $data), Response::HTTP_OK);
    }

    /**
     * @Rest\Delete("/{id}")
     */
    public function delete(WeekendCategory $weekendCategory, WeekendCategoryRepository $repository): View
    {
        $repository->remove($weekendCategory, true);

        return View::create('Category deleted', Response::HTTP_OK);
    }

    /**
     * @Rest\Post
     * @ParamConverter("weekendCategory", converter="fos_rest.request_body")
     */
    public function save(WeekendCategory $weekendCategory, WeekendCategoryRepository $repository): View
    {
        $repository->add($weekendCategory, true);
        return View::create($weekendCategory, Response::HTTP_OK);
    }

}
