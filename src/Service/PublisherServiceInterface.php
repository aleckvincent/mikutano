<?php

namespace App\Service;

use App\Entity\Core\Publisher;

interface PublisherServiceInterface
{
    /**
     * @param Publisher $publisher
     * @return Publisher
     */
    public function save(Publisher $publisher): Publisher;

    /**
     * @param int $id
     * @param string $data
     * @return Publisher
     */
    public function update(int $id, string $data): Publisher;

    /**
     * @return Publisher[]
     */
    public function list(): array;

    public function remove(Publisher $publisher): void;

}
