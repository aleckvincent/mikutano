<?php

namespace App\Service;

use App\Entity\Core\User;
use App\Exception\MissingMandatoryPropertyException;
use App\Exception\PasswordLengthException;
use App\Exception\ResourceNotFoundException;

interface UserServiceInterface
{

    /**
     * Register an user
     * @param User $user
     * @return User
     * @throws MissingMandatoryPropertyException
     */
    public function register(User $user): User;

    /**
     * Activate a user using his id
     * @param string $token
     * @param string $password
     * @return User
     * @throws ResourceNotFoundException
     */
    public function activate(string $token, string $password): User;

    /**
     * @param string $email
     * @return User
     * @throws ResourceNotFoundException
     */
    public function askForgotPassword(string $email): User;

    /**
     * Reset password (forgot password)
     * @param string $password
     * @param string $forgotToken
     * @return User
     * @throws ResourceNotFoundException
     * @throws PasswordLengthException
     */
    public function resetForgotPassword(string $password, string $forgotToken): User;

    /**
     * Update the user
     * @param int $id
     * @param string $data
     * @return User
     * @throws ResourceNotFoundException
     */
    public function update(int $id, string $data): User;

    /**
     * Anonymization user
     * @param User $user
     * @return void
     */
    public function delete(User $user): void;

    /**
     * Return all users (except deleted users)
     * @return User[]
     */
    public function list(): array;

    /**
     * Return user (if not deleted)
     * @param User $user
     * @return User
     */
    public function show(User $user): User;

}
