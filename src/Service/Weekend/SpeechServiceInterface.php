<?php

namespace App\Service\Weekend;

use App\Entity\Weekend\Speech;

interface SpeechServiceInterface
{

    public function autocomplete($key = null): array;

    public function update(Speech $speech, string $data): Speech;
}
