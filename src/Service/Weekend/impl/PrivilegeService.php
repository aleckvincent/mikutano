<?php

namespace App\Service\Weekend\impl;

use App\Entity\Core\Publisher;
use App\Entity\Weekend\Privilege;
use App\Exception\ResourceNotFoundException;
use App\Repository\Core\PublisherRepository;
use App\Service\Weekend\PrivilegeServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class PrivilegeService implements PrivilegeServiceInterface
{

    private PublisherRepository $publisherRepository;

    private EntityManagerInterface $em;

    private SerializerInterface $serializer;

    /**
     * @param PublisherRepository $publisherRepository
     * @param EntityManagerInterface $em
     * @param SerializerInterface $serializer
     */
    public function __construct(PublisherRepository $publisherRepository, EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->publisherRepository = $publisherRepository;
        $this->em = $em;
        $this->serializer = $serializer;
    }


    public function save(int $publisherId, Privilege $privilege): Privilege
    {
        $publisher = $this->publisherRepository->find($publisherId);

        if (!$publisher) {
            throw new ResourceNotFoundException('Publisher not found with id : ' . $publisherId);
        }

        $privilege->setPublisher($publisher);

        $this->em->persist($privilege);
        $this->em->flush();

        return $privilege;

    }

    public function update(Privilege $privilege, string $data): Privilege
    {
        $this->serializer->deserialize($data, Privilege::class, 'json', ['object_to_populate' => $privilege]);
        $this->em->flush();

        return $privilege;
    }
}
