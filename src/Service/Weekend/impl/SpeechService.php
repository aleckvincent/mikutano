<?php

namespace App\Service\Weekend\impl;

use App\Entity\Weekend\Speech;
use App\Repository\Weekend\SpeechRepository;
use App\Service\Weekend\SpeechServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class SpeechService implements SpeechServiceInterface
{

    private SpeechRepository $repository;

    private EntityManagerInterface $em;

    private SerializerInterface $serializer;

    public function __construct(SpeechRepository $repository, EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->serializer = $serializer;
    }

    public function autocomplete($key = null): array
    {
        return $this->repository->autocompletion($key);
    }

    public function update(Speech $speech, string $data): Speech
    {
        $this->serializer->deserialize($data, Speech::class, 'json', ['object_to_populate' => $speech]);
        $this->em->flush();

        return $speech;
    }
}
