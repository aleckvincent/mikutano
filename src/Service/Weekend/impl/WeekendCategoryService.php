<?php

namespace App\Service\Weekend\impl;

use App\Entity\Weekend\WeekendCategory;
use App\Service\Weekend\WeekendCategoryServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class WeekendCategoryService implements WeekendCategoryServiceInterface
{

    private SerializerInterface $serializer;

    private EntityManagerInterface $em;

    public function __construct(SerializerInterface $serializer, EntityManagerInterface $em)
    {
        $this->serializer = $serializer;
        $this->em = $em;
    }

    public function update(WeekendCategory $weekendCategory, string $data): WeekendCategory
    {
        $this->serializer->deserialize($data, WeekendCategory::class, 'json', ['object_to_populate' => $weekendCategory]);
        $this->em->flush();

        return $weekendCategory;
    }
}
