<?php

namespace App\Service\Weekend;

use App\Entity\Weekend\WeekendCategory;

interface WeekendCategoryServiceInterface
{
    public function update(WeekendCategory $weekendCategory, string $data): WeekendCategory;
}
