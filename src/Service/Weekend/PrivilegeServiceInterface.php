<?php

namespace App\Service\Weekend;

use App\Entity\Weekend\Privilege;

interface PrivilegeServiceInterface
{

    public function save(int $publisherId, Privilege $privilege): Privilege;

    public function update(Privilege $privilege, string $data): Privilege;

}
