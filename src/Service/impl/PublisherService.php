<?php

namespace App\Service\impl;

use App\Entity\Core\Publisher;
use App\Exception\MissingMandatoryPropertyException;
use App\Exception\ResourceNotFoundException;
use App\Repository\Core\PublisherRepository;
use App\Service\PublisherServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PublisherService implements PublisherServiceInterface
{

    private ValidatorInterface $validator;

    private EntityManagerInterface $em;

    private PublisherRepository $repository;

    private SerializerInterface $serializer;

    public function __construct(ValidatorInterface $validator,
                                EntityManagerInterface $em,
                                SerializerInterface $serializer,
                                PublisherRepository $repository)
    {
        $this->validator = $validator;
        $this->em = $em;
        $this->repository = $repository;
        $this->serializer = $serializer;
    }

    /**
     * @inheritDoc
     */
    public function save(Publisher $publisher): Publisher
    {
        $errors = $this->validator->validate($publisher);

        if ($errors->count() > 0) {
            throw new MissingMandatoryPropertyException((string) $errors);
        }

        $this->em->persist($publisher);
        $this->em->flush();

        return $publisher;
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, string $data): Publisher
    {
        $publisher = $this->repository->find($id);

        if (!$publisher) {
            throw new ResourceNotFoundException('The publisher with id :' . $id . ' was not found');
        }

        $this->serializer->deserialize($data, Publisher::class, 'json', ['object_to_populate' => $publisher]);

        $errors = $this->validator->validate($publisher);

        $this->em->flush();

        return $publisher;
    }


    /**
     * @inheritDoc
     */
    public function list(): array
    {
        return $this->repository->findBy(['deletedAt' => null], ['lastName' => 'ASC']);
    }

    public function remove(Publisher $publisher): void
    {
        $uuid = Uuid::uuid4();

        $publisher->setFirstName($uuid->toString());
        $publisher->setLastName($uuid->toString());
        $publisher->setDeletedAt(new \DateTimeImmutable());
        $this->em->flush();
    }
}
