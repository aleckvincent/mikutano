<?php

namespace App\Service\impl;

use App\Entity\Core\User;
use App\Exception\MissingMandatoryPropertyException;
use App\Exception\PasswordLengthException;
use App\Exception\ResourceNotFoundException;
use App\Repository\Core\UserRepository;
use App\Service\UserServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserService implements UserServiceInterface
{

    private EntityManagerInterface $em;

    private ValidatorInterface $validator;

    private UserPasswordHasherInterface $hasher;

    private MailerInterface $mailer;

    private UserRepository $repository;

    private ParameterBagInterface $parameterBag;

    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $em,
                                ValidatorInterface $validator,
                                UserPasswordHasherInterface $hasher,
                                MailerInterface $mailer,
                                UserRepository $repository,
                                ParameterBagInterface $parameterBag,
                                SerializerInterface $serializer
    )
    {
        $this->em = $em;
        $this->validator = $validator;
        $this->hasher = $hasher;
        $this->mailer = $mailer;
        $this->repository = $repository;
        $this->parameterBag = $parameterBag;
        $this->serializer = $serializer;
    }

    /**
     * @inheritDoc
     * @throws TransportExceptionInterface
     */
    public function register(User $user): User
    {
        $errors = $this->validator->validate($user);

        if (count($errors) > 0) {
            throw new MissingMandatoryPropertyException((string)$errors);
        }

        //hash the default password
        $hash = $this->hasher->hashPassword($user, $this->parameterBag->get('default_password'));
        $user->setPassword($hash);

        //set activation token
        $uuid = Uuid::uuid4();
        $user->setActivationToken($uuid->toString());

        $this->em->persist($user);
        $this->em->flush();

        $email = (new TemplatedEmail())
            ->to($user->getEmail())
            ->priority(Email::PRIORITY_HIGH)
            ->subject('Création de ton compte Mikutano')
            ->htmlTemplate('mailer/user/register.html.twig')
            ->context([
                'user' => $user
            ]);

        $this->mailer->send($email);

        return $user;
    }


    /**
     * @inheritDoc
     */
    public function activate(string $token, string $password): User
    {
        $user  = $this->repository->findOneBy(['activation_token' => $token]);

        if (!$user) {
            throw new ResourceNotFoundException('The user was not found');
        }

        //hash the password
        $hash = $this->hasher->hashPassword($user, $password);
        $user->setPassword($hash);


        $user->setIsActive(true);

        $this->em->flush();

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function askForgotPassword(string $email): User
    {
        $user  = $this->repository->findOneBy(['email' => $email]);

        if (!$user) {
            throw new ResourceNotFoundException('The user was not found');
        }

        $uuid = Uuid::uuid4();

        $user->setForgotToken($uuid->toString());

        $this->em->flush();

        $email = (new TemplatedEmail())
            ->to($user->getEmail())
            ->priority(Email::PRIORITY_HIGH)
            ->subject('Réinitialisation de ton mot de passe')
            ->htmlTemplate('mailer/user/reset.html.twig')
            ->context([
                'user' => $user
            ]);

        $this->mailer->send($email);

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function resetForgotPassword(string $password, string $forgotToken): User
    {
        $user = $this->repository->findOneBy(['forgotToken' => $forgotToken]);

        if (!$user) {
            throw new ResourceNotFoundException('The user was not found');
        }

        if (strlen($password) < 8) {
            throw new PasswordLengthException('The password must contain at least 8 characters');
        }

        $user->setPassword($this->hasher->hashPassword($user, $password));
        $user->setForgotToken(null);

        $this->em->flush();

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, string $data): User
    {

        $user = $this->repository->find($id);

        if (!$user) {
            throw new ResourceNotFoundException('The user with id : ' . $id . ' was not found.');
        }

        $this->serializer->deserialize($data, User::class, 'json', ['object_to_populate' => $user]);

        $errors = $this->validator->validate($user);

        if (count($errors) > 0) {
            throw new MissingMandatoryPropertyException((string) $errors);
        }

        $this->em->flush();

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function delete(User $user): void
    {
        $uuid = Uuid::uuid4();

        $user->setFirstName(substr($uuid, 0, 60));
        $user->setLastName(substr($uuid, 0, 60));
        $user->setEmail(substr($uuid, 0, 60));
        $user->setDeletedAt(new \DateTimeImmutable());
        $user->setIsActive(false);

        $this->em->flush();
    }

    /**
     * @inheritDoc
     */
    public function list(): array
    {
        return $this->repository->findBy(['deletedAt' => null]);
    }

    /**
     * @inheritDoc
     */
    public function show(User $user): User
    {
        if ($user->getDeletedAt()) {
            throw new ResourceNotFoundException('You account is deleted. Please ask your administrator.');
        }

        return $user;
    }
}
