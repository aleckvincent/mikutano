<?php

namespace App\DataFixtures;

use App\Entity\Core\Publisher;
use App\Entity\Weekend\Privilege;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PrivilegeFixtures extends Fixture implements DependentFixtureInterface
{


    public function load(ObjectManager $manager): void
    {
        $publisher = $manager->getRepository(Publisher::class)->findOneBy(['firstName' => 'Thierry']);

        $privilege = new Privilege();
        $privilege
            ->setPublisher($publisher)
            ->setIsReader(true);


        $manager->persist($privilege);
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies(): array
    {
        return [
            PublisherFixtures::class
        ];
    }
}
