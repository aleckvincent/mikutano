<?php

namespace App\DataFixtures;

use App\Entity\Core\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{

    protected $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $users = [
            [
                'firstName' => 'Zachariah',
                'lastName' => 'Obed',
                'email' => 'z.o@mikutano.cloud',
                'roles' => ['ROLE_ADMIN', 'ROLE_WEEKEND_VALIDATOR']
            ],
            [
                'firstName' => 'David',
                'lastName' => 'Espe',
                'email' => 'd.e@mikutano.cloud',
                'roles' => ['ROLE_ADMIN', 'ROLE_WEEKEND_VALIDATOR']
            ],
            [
                'firstName' => 'Aleck',
                'lastName' => 'Vincent',
                'email' => 'a.v@mikutano.cloud',
                'deletedAt' => new \DateTimeImmutable(),
                'roles' => ['ROLE_ADMIN', 'ROLE_WEEKEND_VALIDATOR']
            ],
        ];

        foreach ($users as $u) {
            $uuid = Uuid::uuid4();
            $user = new User();
            $user->setFirstName($u['firstName']);
            $user->setLastName($u['lastName']);
            $user->setEmail($u['email']);
            $user->setPassword($this->hasher->hashPassword($user, '123Personnage'));
            $user->setRoles($u['roles']);
            $user->setIsActive(true);
            $user->setActivationToken($uuid->toString());

            if (isset($u['deletedAt'])) {
                $user->setDeletedAt($u['deletedAt']);
                $user->setIsActive(false);
            }

            $manager->persist($user);
        }

        $manager->flush();
    }
}
