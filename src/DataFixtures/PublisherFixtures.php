<?php

namespace App\DataFixtures;

use App\Entity\Core\Publisher;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PublisherFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $publishers = [
            [
                'firstName' => 'Ruben',
                'lastName' => 'Orange',
                'isElder' => true,
                'isServant' => false
            ],
            [
                'firstName' => 'Thierry',
                'lastName' => 'Zimmerman',
                'isElder' => false,
                'isServant' => false
            ]
        ];

        foreach ($publishers as $publisher) {
            $p = new Publisher();
            $p->setFirstName($publisher['firstName']);
            $p->setLastName($publisher['lastName']);
            $p->setIsElder($publisher['isElder']);
            $p->setIsServant($publisher['isServant']);
            $manager->persist($p);
        }

        $manager->flush();
    }
}
