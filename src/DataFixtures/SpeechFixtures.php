<?php

namespace App\DataFixtures;

use App\Entity\Weekend\Speech;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SpeechFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $speeches = [
            [
                'number' => 1,
                'title' => 'Comment résister dans ce monde ?',
                'category' => 'monde, ne pas faire partie'
            ],
            [
                'number' => 2,
                'title' => 'Jeunes, soyez heureux !',
                'category' => 'jeune, famille'
            ],
            [
                'number' => 2,
                'title' => 'Restons intègre',
                'category' => 'monde, ne pas faire partie'
            ]
        ];

        foreach ($speeches as $speech) {
            $s = new Speech();
            $s
                ->setNumber($speech['number'])
                ->setTitle($speech['title'])
                ->setCategory($speech['category'])
                ;

            $manager->persist($s);

        }

        $manager->flush();
    }
}
