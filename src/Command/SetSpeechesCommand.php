<?php

namespace App\Command;

use App\Entity\Weekend\Speech;
use App\Repository\Weekend\SpeechRepository;
use League\Csv\Reader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SetSpeechesCommand extends Command
{
    protected static $defaultName = 'app:set-speeches';
    protected static $defaultDescription = 'Ajout des titres des discours';

    protected SpeechRepository $repository;

    public function __construct(string $name = null, SpeechRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $csv = Reader::createFromPath(__DIR__ . '/speeches_list.csv', 'r');
        $csv->setHeaderOffset(0);
        $csv->setDelimiter(';');
        /** @var array $result */
        $result = iterator_to_array($csv->getRecords());

        for ($i = 1; $i < count($result); $i++) {

            $speech = new Speech();
            $speech->setTitle($result[$i]['title']);
            $speech->setNumber($result[$i]['number']);
            $speech->setCategory(strtolower($result[$i]['category']));
            $this->repository->add($speech, true);
        }



        $io->success('Les discours ont été uploadé');

        return Command::SUCCESS;
    }
}
