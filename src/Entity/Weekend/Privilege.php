<?php

namespace App\Entity\Weekend;

use App\Entity\Core\Publisher;
use App\Repository\Weekend\PrivilegeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PrivilegeRepository::class)
 */
class Privilege
{
    /**
     * @Groups({"show", "list_publisher"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"show", "list_publisher"})
     * @ORM\Column(type="boolean")
     */
    private $isReader = false;

    /**
     * @Groups({"show", "list_publisher"})
     * @ORM\Column(type="boolean")
     */
    private $isChairman = false;

    /**
     * @Groups({"show", "list_publisher"})
     * @ORM\Column(type="boolean")
     */
    private $isSpeaker = false;

    /**
     * @Groups({"show", "list_publisher"})
     * @ORM\Column(type="boolean")
     */
    private $isExternalSpeaker = false;

    /**
     * @Groups({"show_privilege"})
     * @ORM\OneToOne(targetEntity=Publisher::class, inversedBy="privilege")
     * @ORM\JoinColumn(nullable=false)
     */
    private $publisher;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isIsReader(): ?bool
    {
        return $this->isReader;
    }

    public function setIsReader(bool $isReader): self
    {
        $this->isReader = $isReader;

        return $this;
    }

    public function isIsChairman(): ?bool
    {
        return $this->isChairman;
    }

    public function setIsChairman(bool $isChairman): self
    {
        $this->isChairman = $isChairman;

        return $this;
    }

    public function isIsSpeaker(): ?bool
    {
        return $this->isSpeaker;
    }

    public function setIsSpeaker(bool $isSpeaker): self
    {
        $this->isSpeaker = $isSpeaker;

        return $this;
    }

    public function isIsExternalSpeaker(): ?bool
    {
        return $this->isExternalSpeaker;
    }

    public function setIsExternalSpeaker(bool $isExternalSpeaker): self
    {
        $this->isExternalSpeaker = $isExternalSpeaker;

        return $this;
    }

    public function getPublisher(): ?Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(Publisher $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }
}
