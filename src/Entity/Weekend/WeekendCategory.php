<?php

namespace App\Entity\Weekend;

use App\Repository\Weekend\WeekendCategoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WeekendCategoryRepository::class)
 */
class WeekendCategory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="boolean")
     */
    private $readerEnabled;

    /**
     * @ORM\Column(type="boolean")
     */
    private $chairmanEnabled;

    /**
     * @ORM\Column(type="boolean")
     */
    private $speakerEnabled;

    /**
     * @ORM\Column(type="boolean")
     */
    private $speechEnabled;

    /**
     * @ORM\Column(type="boolean")
     */
    private $externalSpeakerEnabled;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function isReaderEnabled(): ?bool
    {
        return $this->readerEnabled;
    }

    public function setReaderEnabled(bool $readerEnabled): self
    {
        $this->readerEnabled = $readerEnabled;

        return $this;
    }

    public function isChairmanEnabled(): ?bool
    {
        return $this->chairmanEnabled;
    }

    public function setChairmanEnabled(bool $chairmanEnabled): self
    {
        $this->chairmanEnabled = $chairmanEnabled;

        return $this;
    }

    public function isSpeakerEnabled(): ?bool
    {
        return $this->speakerEnabled;
    }

    public function setSpeakerEnabled(bool $speakerEnabled): self
    {
        $this->speakerEnabled = $speakerEnabled;

        return $this;
    }

    public function isSpeechEnabled(): ?bool
    {
        return $this->speechEnabled;
    }

    public function setSpeechEnabled(bool $speechEnabled): self
    {
        $this->speechEnabled = $speechEnabled;

        return $this;
    }

    public function isExternalSpeakerEnabled(): ?bool
    {
        return $this->externalSpeakerEnabled;
    }

    public function setExternalSpeakerEnabled(bool $externalSpeakerEnabled): self
    {
        $this->externalSpeakerEnabled = $externalSpeakerEnabled;

        return $this;
    }
}
