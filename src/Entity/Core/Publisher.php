<?php

namespace App\Entity\Core;

use App\Entity\Weekend\Privilege;
use App\Repository\Core\PublisherRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PublisherRepository::class)
 */
class Publisher
{
    /**
     * @Groups({"show_privilege", "show_publisher"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"show_privilege", "show_publisher"})
     * @ORM\Column(type="string", length=128)
     */
    private $firstName;

    /**
     * @Groups({"show_privilege", "show_publisher"})
     * @ORM\Column(type="string", length=128)
     */
    private $lastName;

    /**
     * @Groups({"show_privilege", "show_publisher"})
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isElder;

    /**
     * @Groups({"show_privilege", "show_publisher"})
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isServant;

    /**
     * @Groups({"show_privilege", "show_publisher"})
     * @var string
     */
    private string $initials;

    /**
     * @Groups({"show_privilege", "show_publisher"})
     * @var string
     */
    private string $fullName;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $deletedAt;

    /**
     * @Groups({"list_publisher"})
     * @ORM\OneToOne(targetEntity=Privilege::class, mappedBy="publisher")
     */
    private $privilege;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function isIsElder(): ?bool
    {
        return $this->isElder;
    }

    public function setIsElder(?bool $isElder): self
    {
        $this->isElder = $isElder;

        return $this;
    }

    public function isIsServant(): ?bool
    {
        return $this->isServant;
    }

    public function setIsServant(?bool $isServant): self
    {
        $this->isServant = $isServant;

        return $this;
    }

    /**
     * @return string
     */
    public function getInitials(): string
    {
        $intials = substr($this->firstName, 0, 1). substr($this->lastName, 0, 1);
        return mb_convert_encoding($intials, 'UTF-8', 'ISO-8859-1');
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getPrivilege(): ?Privilege
    {
        return $this->privilege;
    }

    public function setPrivilege(Privilege $privilege): self
    {
        // set the owning side of the relation if necessary
        if ($privilege->getPublisher() !== $this) {
            $privilege->setPublisher($this);
        }

        $this->privilege = $privilege;

        return $this;
    }
}
