<?php

namespace App\Repository\Weekend;

use App\Entity\Weekend\Speech;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Speech>
 *
 * @method Speech|null find($id, $lockMode = null, $lockVersion = null)
 * @method Speech|null findOneBy(array $criteria, array $orderBy = null)
 * @method Speech[]    findAll()
 * @method Speech[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpeechRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Speech::class);
    }

    public function add(Speech $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Speech $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function autocompletion(?string $key)
    {
        $qb = $this->createQueryBuilder('s');

        if ($key) {
            $qb
                ->where('s.number = :key')
                ->orWhere('s.title LIKE :key')
                ->orWhere('s.category LIKE :key')
                ->setParameter('key', "%{$key}%");
        }

        return $qb
            ->orderBy('s.number', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
