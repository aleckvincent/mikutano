<?php

namespace App\Tests\Weekend\Service;

use App\Repository\Weekend\SpeechRepository;
use App\Service\Weekend\impl\SpeechService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SpeechTest extends KernelTestCase
{
    protected $service;
    protected $repository;

    protected function setUp(): void
    {
        $container = static::getContainer();
        $this->service = $container->get(SpeechService::class);
        $this->repository = $container->get(SpeechRepository::class);
    }

    public function testAutocomplete()
    {
        $speeches = $this->service->autocomplete('monde');
        $this->assertCount(2, $speeches);
    }

    public function testUpdate()
    {
        $speech = $this->repository->findOneBy(['number' => 2]);
        $data = ['title' => 'Jeune, soyez joyeux au sein du peuple de Jéhovah'];
        $data = json_encode($data);
        $result = $this->service->update($speech, $data);
        $speechFromDb = $this->repository->findOneBy(['number' => 2]);

        $this->assertSame($result->getId(), $speechFromDb->getId());
        $this->assertSame('Jeune, soyez joyeux au sein du peuple de Jéhovah', $result->getTitle());
        $this->assertSame('Jeune, soyez joyeux au sein du peuple de Jéhovah', $speechFromDb->getTitle());
    }

}
