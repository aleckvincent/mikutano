<?php

namespace App\Tests\Core\Service;

use App\Entity\Core\User;
use App\Exception\MissingMandatoryPropertyException;
use App\Exception\PasswordLengthException;
use App\Exception\ResourceNotFoundException;
use App\Repository\Core\UserRepository;
use App\Service\impl\UserService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTest extends KernelTestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
    }

    public function testUserCreated(): void
    {
        $container = static::getContainer();

        $userService = $container->get(UserService::class);

        $user = new User();
        $user->setEmail('acme@mikutano.cloud');
        $user->setPassword('P!SfDev12');
        $user->setFirstName('John');
        $user->setLastName('Doe');

        $userCreated = $userService->register($user);


        $this->assertSame('John', $userCreated->getFirstName());
        $this->assertSame('Doe', $userCreated->getLastName());
        $this->assertSame('acme@mikutano.cloud', $userCreated->getEmail());
        $this->assertContains('ROLE_USER', $userCreated->getRoles());
        $this->assertNotNull($user->getId());
        $this->assertFalse($userCreated->isActive());
    }

    public function testUserRegistrationFailedMissingEmail(): void
    {
        $container = static::getContainer();

        $userService = $container->get(UserService::class);

        $user = new User();
        $user->setPassword('P!SfDev12');
        $user->setFirstName('John');
        $user->setLastName('Doe');
        $this->expectException(MissingMandatoryPropertyException::class);
        $userService->register($user);
    }

    public function testUserRegistrationPasswordLengthFail(): void
    {
        $container = static::getContainer();

        $userService = $container->get(UserService::class);

        $user = new User();
        $user->setPassword('P!SfDe');
        $user->setEmail('acme@mikutano.cloud');
        $user->setFirstName('John');
        $user->setLastName('Doe');
        $this->expectException(MissingMandatoryPropertyException::class);
        $userService->register($user);
    }

    public function testUserActivation(): void
    {
        $container = static::getContainer();
        $userService = $container->get(UserService::class);

        $userRepository = $container->get(UserRepository::class);
        $zacharia = $userRepository->findOneBy(['firstName' => 'Zachariah']);

        $result = $userService->activate($zacharia->getActivationToken(), 'Bvuojg456Prt');

        $this->assertTrue($result->isActive());
    }

    public function testUserAskPasswordForgotNotFound(): void
    {
        $container = static::getContainer();
        $userService = $container->get(UserService::class);

        $this->expectException(ResourceNotFoundException::class);
        $userService->askForgotPassword('notexist@mikutano.io');
    }

    public function testUserAskPasswordToken(): void
    {
        $container = static::getContainer();
        $userService = $container->get(UserService::class);

        $user = $userService->askForgotPassword('z.o@mikutano.cloud');

        $this->assertNotNull($user->getForgotToken());
    }

    public function testUserPasswordResetLengthTooShort(): void
    {
        $container = static::getContainer();
        $userService = $container->get(UserService::class);
        $zacharia = $userService->askForgotPassword('z.o@mikutano.cloud');

        $this->expectException(PasswordLengthException::class);
        $userService->resetForgotPassword('short', $zacharia->getForgotToken());
    }

    public function testUserPasswordResetSucceed(): void
    {
        $container = static::getContainer();
        $userService = $container->get(UserService::class);

        $zacharia = $userService->askForgotPassword('z.o@mikutano.cloud');

        $user = $userService->resetForgotPassword('JeSuisUnPassword!AssezLong', $zacharia->getForgotToken());
        $this->assertNull($user->getForgotToken());
    }

    public function testUserUpdateFirstName(): void
    {

        $container = static::getContainer();
        $userService = $container->get(UserService::class);
        $userRepository = $container->get(UserRepository::class);

        $david = $userRepository->findOneBy(['firstName' => 'David']);
        $david->setFirstName('Salomon');

        $result = $userService->update($david->getId(), json_encode($david));

        $userFromDb = $userRepository->findOneBy(['firstName' => 'Salomon']);
        $user2FromDb = $userRepository->findOneBy(['firstName' => 'David']);

        $this->assertEquals('Salomon', $result->getFirstName());
        $this->assertSame($david->getId(), $result->getId());
        $this->assertNull($user2FromDb);
        $this->assertNotNull($userFromDb);
    }

    public function testUserShowIsDeleted(): void
    {

        $container = static::getContainer();
        $userService = $container->get(UserService::class);
        $userRepository = $container->get(UserRepository::class);

        $user = $userRepository->findOneBy(['firstName' => 'Aleck']);

        $this->expectException(ResourceNotFoundException::class);

        $userService->show($user);
    }

}
