<?php

namespace App\Tests\Core\Service;

use App\Entity\Core\Publisher;
use App\Repository\Core\PublisherRepository;
use App\Service\PublisherServiceInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PublisherTest extends KernelTestCase
{

    protected $service;

    protected function setUp(): void
    {
        $this->service = static::getContainer()->get(PublisherServiceInterface::class);
    }

    public function testPublisherCreated()
    {
        $publisher = new Publisher();
        $publisher->setFirstName('John');
        $publisher->setLastName('Doe');
        $publisher->setIsElder(false);
        $publisher->setIsServant(true);

        $publisherCreated = $this->service->save($publisher);
        $this->assertNotNull($publisherCreated->getId());
    }

    public function testPublisherUpdated()
    {
        $repository = static::getContainer()->get(PublisherRepository::class);
        $publisher = $repository->findOneBy(['firstName' => 'Ruben']);
        $publisher->setFirstName('Kévin');

        $result = $this->service->update($publisher->getId(), json_encode($publisher));
        $resulFromDbNull = $repository->findOneBy(['firstName' => 'Ruben']);
        $resultFromDbTrue = $repository->findOneBy(['firstName' => 'Kévin']);

        $this->assertNull($resulFromDbNull);
        $this->assertEquals('Kévin', $resultFromDbTrue->getFirstName());
        $this->assertEquals($publisher->getId(), $resultFromDbTrue->getId());
    }

}
