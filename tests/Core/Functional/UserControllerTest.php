<?php

namespace App\Tests\Core\Functional;

use App\Repository\Core\UserRepository;
use App\Service\impl\UserService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{

    protected $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('d.e@mikutano.cloud');

        // simulate $testUser being logged in
        $this->client->loginUser($testUser);
    }

    public function testUserRegistered(): void
    {

        $fileName = 'test-user-registered.json';
        $json = $this->jsonReader($fileName);

       $crawler = $this->client->request('POST', '/api/users', [], [], ['CONTENT_TYPE' => 'application/json'], $json);
       $response = json_decode($this->client->getResponse()->getContent());
       $this->assertResponseIsSuccessful();
       $this->assertNotNull($response->id);
    }

    public function testUserActivation(): void
    {
        $fileName = 'test-user-activation.json';
        $json = $this->jsonReader($fileName);

        $container = static::getContainer();
        $userRepository = $container->get(UserRepository::class);
        $zacharia = $userRepository->findOneBy(['firstName' => 'Zachariah']);
        $crawler = $this->client->request('POST', '/api/users/activate/' . $zacharia->getActivationToken(), [], [], ['CONTENT_TYPE' => 'application/json'], $json);
        $response = json_decode($this->client->getResponse()->getContent());
        $this->assertResponseIsSuccessful();
        $this->assertNotNull($response->id);
        $this->assertTrue($response->isActive);
    }

    public function testUserAskForgotPasswordNotFound(): void
    {
        $fileName = 'test-user-ask-forgot-not-found.json';
        $json = $this->jsonReader($fileName);


        $this->client->request('POST', '/api/users/forgot/ask?email=aleck', [], [], ['CONTENT_TYPE' => 'application/json'], $json);
        $this->assertResponseStatusCodeSame(404);
    }

    public function testUserAskForgotPasswordSucceed(): void
    {
        $fileName = 'test-user-ask-forgot-succeed.json';
        $json = $this->jsonReader($fileName);

        $this->client->request('POST', '/api/users/forgot/ask', [], [], ['CONTENT_TYPE' => 'application/json'], $json);
        $this->assertResponseIsSuccessful();
    }

    public function testUserResetForgotPasswordSucceed(): void
    {

        $fileName = 'test-user-reset-forgot-succeed.json';
        $json = $this->jsonReader($fileName);

        $container = static::getContainer();
        $userService = $container->get(UserService::class);
        $zacharia = $userService->askForgotPassword('z.o@mikutano.cloud');

        $this->client->request('POST', '/api/users/forgot/reset/' . $zacharia->getForgotToken(), [], [], ['CONTENT_TYPE' => 'application/json'], $json);
        $this->assertResponseIsSuccessful();
    }

    public function testUserUpdateFirstName(): void
    {
        $container = static::getContainer();
        $userRepository = $container->get(UserRepository::class);
        $david = $userRepository->findOneBy(['firstName' => 'David']);

        $david->setFirstName('Vivech');

        $json = json_encode($david);

        $this->client->request('PUT', '/api/users/' . $david->getId() , [], [], ['CONTENT_TYPE' => 'application/json'], $json);

        $userFromDb = $userRepository->findOneBy(['firstName' => 'Vivech']);

        $this->assertEquals($david->getId(), $userFromDb->getId());
    }

    public function testUserDelete(): void
    {
        $container = static::getContainer();
        $userRepository = $container->get(UserRepository::class);
        $david = $userRepository->findOneBy(['firstName' => 'David']);

        $this->client->request('DELETE', '/api/users/' . $david->getId());

        $userFromDb = $userRepository->findOneBy(['id' => $david->getId()]);


        $this->assertEquals($david->getId(), $userFromDb->getId());
        $this->assertFalse($userFromDb->isActive());
        $this->assertNotNull($userFromDb->getDeletedAt());
        $this->assertNotEquals('David', $userFromDb->getFirstName());
        $this->assertNotEquals('Espe', $userFromDb->getLastName());
        $this->assertNotEquals('d.e@mikutano.cloud', $userFromDb->getEmail());
    }

    public function testUsersList(): void
    {

        $this->client->request('GET', '/api/users');

        $response = json_decode($this->client->getResponse()->getContent());

        $this->assertResponseIsSuccessful();
        $this->assertCount(2, $response);
    }

    public function testUserShow(): void
    {
        $container = static::getContainer();
        $userRepository = $container->get(UserRepository::class);
        $david = $userRepository->findOneBy(['firstName' => 'David']);

        $this->client->request('GET', '/api/users/' . $david->getId());

        $response = json_decode($this->client->getResponse()->getContent());

        $this->assertResponseIsSuccessful();
        $this->assertEquals($david->getFirstName(), $response->firstName);
        $this->assertEquals($david->getId(), $response->id);
        $this->assertEquals($david->getEmail(), $response->email);
    }


    private function jsonReader(string $fileName): string
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'requests' . DIRECTORY_SEPARATOR . $fileName;

        return file_get_contents($path);
    }
}
