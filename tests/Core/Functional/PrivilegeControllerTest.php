<?php

namespace App\Tests\Core\Functional;

use App\Repository\Core\PublisherRepository;
use App\Repository\Core\UserRepository;
use App\Repository\Weekend\PrivilegeRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PrivilegeControllerTest extends WebTestCase
{
    protected $client;

    protected $publisherRepository;

    protected $privilegeRepository;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $this->publisherRepository = static::getContainer()->get(PublisherRepository::class);
        $this->privilegeRepository = static::getContainer()->get(PrivilegeRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('d.e@mikutano.cloud');

        // simulate $testUser being logged in
        $this->client->loginUser($testUser);
    }

    private function jsonReader(string $fileName): string
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'requests' . DIRECTORY_SEPARATOR . 'privilege' . DIRECTORY_SEPARATOR . $fileName;

        return file_get_contents($path);
    }

    public function testPrivilegeCreation()
    {
        $json = $this->jsonReader('test-privilege-creation.json');
        $publisher = $this->publisherRepository->findOneBy(['firstName' => 'Ruben']);

        $this->client->request('POST', '/api/privileges/publisher/' . $publisher->getId(), [], [], ['CONTENT_TYPE' => 'application/json'], $json);

        $response = json_decode($this->client->getResponse()->getContent());
        $this->assertSame($response->publisher->id, $publisher->getId());
        $this->assertTrue($response->isReader);
        $this->assertFalse($response->isChairman);
        $this->assertFalse($response->isSpeaker);
        $this->assertFalse($response->isExternalSpeaker);
    }

    public function testPrivilegeUpdate()
    {
        $json = $this->jsonReader('test-privilege-update.json');
        $publisher = $this->publisherRepository->findOneBy(['firstName' => 'Thierry']);
        $privilege = $this->privilegeRepository->findAll();

        $this->client->request('PUT', '/api/privileges/' . $privilege[0]->getId(), [], [], ['CONTENT_TYPE' => 'application/json'], $json);

        $response = json_decode($this->client->getResponse()->getContent());

        $this->assertResponseIsSuccessful();
        $this->assertSame($response->publisher->id, $publisher->getId());
        $this->assertTrue($response->isReader);
        $this->assertFalse($response->isChairman);
        $this->assertTrue($response->isSpeaker);
        $this->assertTrue($response->isExternalSpeaker);
    }

}
