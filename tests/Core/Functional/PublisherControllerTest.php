<?php

namespace App\Tests\Core\Functional;

use App\Repository\Core\PublisherRepository;
use App\Repository\Core\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PublisherControllerTest extends WebTestCase
{

    protected $client;

    protected $repository;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $this->repository = static::getContainer()->get(PublisherRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('d.e@mikutano.cloud');

        // simulate $testUser being logged in
        $this->client->loginUser($testUser);
    }

    private function jsonReader(string $fileName): string
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'requests' . DIRECTORY_SEPARATOR . 'publisher' . DIRECTORY_SEPARATOR . $fileName;

        return file_get_contents($path);
    }

    public function testPublisherCreated()
    {
        $json = $this->jsonReader('test-publisher-created.json');

        $this->client->request('POST', '/api/publishers', [], [], ['CONTENT_TYPE' => 'application/json'], $json);
        $resultFromDb = $this->repository->findOneBy(['firstName' => 'Aleck']);
        $response = json_decode($this->client->getResponse()->getContent());

        $this->assertSame($response->id, $resultFromDb->getId());
    }

    public function testPublisherUpdated()
    {
        $json = $this->jsonReader('test-publisher-updated.json');
        $pubFromDb = $this->repository->findOneBy(['firstName' => 'Ruben']);

        $this->client->request('PUT', '/api/publishers/' . $pubFromDb->getId(), [], [], ['CONTENT_TYPE' => 'application/json'], $json);
        $resultFromDb = $this->repository->findOneBy(['firstName' => 'Ruben']);
        $response = json_decode($this->client->getResponse()->getContent());

        $this->assertSame($response->id, $resultFromDb->getId());
        $this->assertSame('Vincent-Minatchy', $resultFromDb->getLastName());
        $this->assertSame('Vincent-Minatchy', $response->lastName);
    }

}
