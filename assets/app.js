import Vue from 'vue';
import App from "./vue/App.vue";
import VueRouter from 'vue-router';
import Toast from 'vue-toastification';
Vue.use(VueRouter);
Vue.use(Toast);

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/app/users', name: 'users_list', component: require('./vue/pages/users/List.vue').default },
        { path: '/app/users/show/:id', name: 'user_show', component: require('./vue/pages/users/Show.vue').default },
        { path: '/app/activation/:id', name: 'user_activation', component: require('./vue/pages/users/Activation.vue').default, meta: { fullTemplate: false } },
        { path: '/app/forgot/password', name: 'user_ask_password', component: require('./vue/pages/users/AskPassword.vue').default, meta: { fullTemplate: false } },
        { path: '/app/forgot/reset/password/:token', name: 'user_reset_forgot_password', component: require('./vue/pages/users/Activation.vue').default, meta: { fullTemplate: false, resetPassword: true } },
        { path: '/app/publishers', name: 'publishers_list', component: require('./vue/pages/publishers/List.vue').default, meta: { fullTemplate: true } },
        { path: '/app/publishers/show/:id', name: 'publisher_show', component: require('./vue/pages/publishers/Show.vue').default, meta: { fullTemplate: true } },
        { path: '/app/weekend/privileges', name: 'weekend_privileges_list', component: require('./vue/pages/weekend/privileges/List.vue').default, meta: { fullTemplate: true } },
        { path: '/app/weekend/speeches', name: 'weekend_speeches_list', component: require('./vue/pages/weekend/speeches/List.vue').default, meta: { fullTemplate: true } },
        { path: '/app/weekend/speeches/show/:id', name: 'weekend_speeches_show', component: require('./vue/pages/weekend/speeches/Show.vue').default, meta: { fullTemplate: true } },
        { path: '/app/weekend/categories/', name: 'weekend_categories_list', component: require('./vue/pages/weekend/categories/List.vue').default, meta: { fullTemplate: true } },
    ]
})


new Vue({
    el: '#vue-app',
    router,
    render: h => h(App)
});

